package tensorflow.com.tensorflow;

import android.Manifest;
import android.animation.Animator;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class CameraView extends AppCompatActivity {
    private static final int CAMERA_PERMISSION_RESULT = 0;
    private static final int STORAGE_PERMISSION_RESULT = 0;

    private FloatingActionButton flp;
    private ProgressBar prbr;
    private ImageView imageView;
    private ImageView capturBlink;
    private ImageReader imageReader;

    private Animation captureAnimation;

    private int width = 68;
    private int height = 52;

    private Size cameraViewSize;
    private CaptureRequest.Builder captureRequestBuilder;

    private Bitmap currentFrame;
    private String cameraId;

    private File imageStorageFolder;

    private Handler backgroundHelperHandler;
    private HandlerThread backgroundHelperThread;

    private Handler backgroundHelperHandler2;
    private HandlerThread backgroundHelperThread2;

    private TensorflowModel tensorflowModel;

    private CameraDevice camera;
    private CameraDevice.StateCallback cameraCallbackListener = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            camera = cameraDevice;
            startPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            closeCamera();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            closeCamera();
        }
    };

    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static{
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_view);

        createImageFolder();

        flp = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        prbr = (ProgressBar) findViewById(R.id.progressBar);
        imageView = (ImageView) findViewById(R.id.imageView);
        capturBlink = (ImageView) findViewById(R.id.captureBlink);
        capturBlink.setAlpha(0f);

        flp.setVisibility(View.GONE);
        flp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStoragePermissionsAndCapture();
                capturBlink.animate().alpha(0.1f).setDuration(20).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        capturBlink.animate().alpha(0).setDuration(50);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

            }
        });


        tensorflowModel = new TensorflowModel(getAssets());

        //setup animations

        captureAnimation = new AlphaAnimation(0.0f, 1.0f);
        captureAnimation.setDuration(1000);
        captureAnimation.setStartOffset(20);
        captureAnimation.setInterpolator(new LinearInterpolator());
        captureAnimation.setRepeatMode(Animation.REVERSE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startHelperThreads();
        setupCamera(width, height);

        connectCamera();
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopHelperThreads();
        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View decorView = getWindow().getDecorView();
        if(hasFocus){
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    |View.SYSTEM_UI_FLAG_FULLSCREEN
                    |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == CAMERA_PERMISSION_RESULT){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission denied, application will not show real time image!", Toast.LENGTH_LONG);

            }
        }

        if(requestCode == STORAGE_PERMISSION_RESULT){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission denied, image not saved!", Toast.LENGTH_LONG);

            }else{
                captureImage();
            }
        }
    }

    private void setupCamera(int width, int height){
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for(String camId: cameraManager.getCameraIdList()){
                CameraCharacteristics camCharcteristics = cameraManager.getCameraCharacteristics(camId);

                if(camCharcteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK){
                    StreamConfigurationMap map = camCharcteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                    cameraViewSize = optimalCamSize(map.getOutputSizes(ImageFormat.JPEG), width, height);

                    cameraId = camId;
                    return;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startHelperThreads(){
        backgroundHelperThread = new HandlerThread("tensorflow helper thread");
        backgroundHelperThread.start();

        backgroundHelperHandler = new Handler(backgroundHelperThread.getLooper());

        backgroundHelperThread2 = new HandlerThread("tensorflow helper thread 2");
        backgroundHelperThread2.start();

        backgroundHelperHandler2 = new Handler(backgroundHelperThread2.getLooper());
    }

    private void stopHelperThreads(){
        backgroundHelperThread.quitSafely();
        backgroundHelperThread2.quitSafely();

        try {
            backgroundHelperThread.join();
            backgroundHelperThread = null;
            backgroundHelperHandler = null;

            backgroundHelperThread2.join();
            backgroundHelperThread2 = null;
            backgroundHelperHandler2 = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void checkStoragePermissionsAndCapture(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)==
                    PackageManager.PERMISSION_GRANTED){
                captureImage();
            }else{
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "App requires storage permissions to save a photo", Toast.LENGTH_LONG).show();
                }
                requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RESULT);
            }
        }else{
            captureImage();
        }
    }

    private void captureImage(){
        Bitmap a = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        String name = new SimpleDateFormat("yyyy_MMdd_HH:mm:ss:SS").format(new Date());
        name = "TF_image_" + name;

        MediaStore.Images.Media.insertImage(getContentResolver(), a, name , "TF_image");
    }

    private void createImageFolder(){
        File imageFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        imageStorageFolder = new File(imageFolder, "TensorflowImage");
        if(!imageStorageFolder.exists()){
            imageStorageFolder.mkdirs();
        }
    }


    private void connectCamera(){
        CameraManager cm = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED){
                    cm.openCamera(cameraId, cameraCallbackListener, backgroundHelperHandler);
                } else {
                    if(shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                        Toast.makeText(this, "The app requires camera capturing", Toast.LENGTH_LONG).show();
                    }
                    requestPermissions(new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_RESULT);
                }
            }else{
                cm.openCamera(cameraId, cameraCallbackListener, backgroundHelperHandler);
            }

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera(){
        if(camera != null){
            camera.close();
            camera = null;
        }
    }

    private void startPreview(){
        imageReader = ImageReader.newInstance(cameraViewSize.getWidth(), cameraViewSize.getHeight(), ImageFormat.JPEG, 5);
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader imageReader) {

                Image image = imageReader.acquireLatestImage();
                if (image == null)
                    return;

                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.remaining()];
                buffer.get(bytes);

                Matrix m = new Matrix();
                m.postRotate(-270);
                currentFrame = BitmapFactory.decodeByteArray(bytes,0,bytes.length, null);
                currentFrame = Bitmap.createScaledBitmap(currentFrame, TensorflowModel.INPUT_SIZE[0], TensorflowModel.INPUT_SIZE[0], true);
                currentFrame = Bitmap.createBitmap(currentFrame,0,0,currentFrame.getWidth(), currentFrame.getHeight(), m, true);

                currentFrame = tensorflowModel.runNetwork(currentFrame);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(prbr.getVisibility() != View.GONE){
                            prbr.setVisibility(View.GONE);
                            flp.setVisibility(View.VISIBLE);
                        }

                        imageView.setImageBitmap(currentFrame);
                    }
                });


                image.close();
            }
        }, backgroundHelperHandler2);


        try {
            captureRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(imageReader.getSurface());

            camera.createCaptureSession(Arrays.asList(imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback(){

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            try {
                                cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(), null, backgroundHelperHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                            Toast.makeText(getApplicationContext(), "Camera not available!", Toast.LENGTH_LONG);
                        }
                    }
            , null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    private static class CompareSizeByArea implements Comparator<Size> {

        @Override
        public int compare(Size size, Size t1) {
            return Long.signum(((long) size.getWidth() * size.getHeight())/((long)t1.getWidth() * t1.getHeight()) );
        }
    }

    private static Size optimalCamSize(Size[] availableSizes, int width, int height){
            List<Size> optimalSizes = new ArrayList<>();

            for(Size size: availableSizes){
                float ratio = height / width;
                if(size.getWidth() == size.getHeight() * ratio &&
                        size.getWidth() >= width &&
                        size.getHeight() >= height){
                    optimalSizes.add(size);
                }
        }

        if(optimalSizes.size() > 0){
            return Collections.min(optimalSizes, new CompareSizeByArea());
        }else{
            return availableSizes[0];
        }

    }

}
