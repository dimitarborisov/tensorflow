package tensorflow.com.tensorflow;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;


public class TensorflowModel {
    static {
        System.loadLibrary("tensorflow_inference");
    }

    private static final String MODEL_FILE = "file:///android_asset/file_name.h5.pb";
    private static final String INPUT_NODE = "input_2";

    //width, height
    public static final int[] INPUT_SIZE = {32,32};

    //width, height, channels
    public static final int[] OUTPUT_SIZE = {32, 32, 3};
    private static final String OUTPUT_NODE = "output_node0";


    private TensorFlowInferenceInterface inferenceInterface;

    public TensorflowModel(AssetManager assetManager){
        inferenceInterface = new TensorFlowInferenceInterface(assetManager, MODEL_FILE);
    }

    public Bitmap runNetwork(Bitmap bitmap){
        long runShape[] = {1, INPUT_SIZE[0], INPUT_SIZE[1], 3};
        String[] output = {OUTPUT_NODE};

        Bitmap bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        int intArray[] = new int[bmp.getWidth()*bmp.getHeight()];
        float floatArray[] = new float[bmp.getWidth()*bmp.getHeight()*3];

        bmp.getPixels(intArray, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        //normalize data
        for (int i = 0; i < intArray.length; ++i) {
            final int val = intArray[i];
            floatArray[i * 3 + 0] = ((val >> 16) & 0xFF)  / 255.0f;
            floatArray[i * 3 + 1] = ((val >> 8) & 0xFF) / 255.0f;
            floatArray[i * 3 + 2] = (val & 0xFF)  / 255.0f;
        }

        inferenceInterface.feed(INPUT_NODE, floatArray, runShape);

        float[] outputModelImage = new float[OUTPUT_SIZE[0] * OUTPUT_SIZE[1] * OUTPUT_SIZE[2]];

        // Run the TensorFlow Neural Network with the provided input
        inferenceInterface.run(output);

        inferenceInterface.fetch(OUTPUT_NODE, outputModelImage);


        int j = 0;
        for(int i = 0; i < bmp.getWidth()*bmp.getHeight() * 3; i = i + 3){
            intArray[j] = Color.rgb((int)(outputModelImage[i] * 255f), (int)(outputModelImage[i+1] * 255f), (int)(outputModelImage[i+2] * 255f));
            j++;
        }

        return Bitmap.createBitmap( intArray, OUTPUT_SIZE[0], OUTPUT_SIZE[1], Bitmap.Config.ARGB_8888);
    }

}
